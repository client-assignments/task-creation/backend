/**
 * Config
 * */
const config = require('../../config');
module.exports = (mailObject) => {
  return `
  Hello ${mailObject.assigneeName},<br><br>
  
  A task has been created for you. Following are the details of the task:<br><br>
  
  ${
    mailObject.taskTitle !== undefined
      ? `
    Task Title: ${mailObject.taskTitle}<br>
    `
      : ``
  }
  ${
    mailObject.projectName !== undefined
      ? `
    Project: ${mailObject.projectName}<br>
    `
      : ``
  }
  ${
    mailObject.clientName !== undefined
      ? `
    Client: ${mailObject.clientName}<br>
    `
      : ``
  }
  ${
    mailObject.dueDate !== undefined
      ? `
    Due Date: ${mailObject.dueDate}<br>
    `
      : ``
  }
  ${
    mailObject.createdByName !== undefined
      ? `
    Reporter: ${mailObject.createdByName}<br>
    `
      : ``
  }
  ${
    mailObject.priorityName !== undefined
      ? `
    Priority: ${mailObject.priorityName}<br>
    `
      : ``
  }<br>
  <a href=${config.frontendUrl}>Click here</a> to get more details.
`;
};
