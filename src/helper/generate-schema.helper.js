require('dotenv').config();
const Client = require('../models/client.model');
const Priority = require('../models/priority.model');
const Project = require('../models/project.model');
const Status = require('../models/status.model');
const Task = require('../models/task.model');
const Type = require('../models/type.model');
const User = require('../models/user.model');

/**
 * Create tables in database script function
 */
const createTables = async () => {
  try {
    await Client.sync();
    await Priority.sync();
    await Project.sync();
    await Status.sync();
    await Type.sync();
    await User.sync();
    await Task.sync();
    process.exit(0);
  } catch (e) {
    console.log('Error in creating Tables:', e);
  }
};

createTables();
