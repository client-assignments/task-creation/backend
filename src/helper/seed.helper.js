require('dotenv').config();
const Client = require('../models/client.model');
const Priority = require('../models/priority.model');
const Project = require('../models/project.model');
const Status = require('../models/status.model');
const Type = require('../models/type.model');
const User = require('../models/user.model');
const clientJson = require('../../seed/client.json');
const userJson = require('../../seed/user.json');
const priorityJson = require('../../seed/priority.json');
const projectJson = require('../../seed/project.json');
const statusJson = require('../../seed/status.json');
const typeJson = require('../../seed/type.json');

/**
 * Add data in the database table
 */
const seedData = async () => {
  try {
    await seedClients();
    await seedUsers();
    await seedStatuses();
    await seedPriorities();
    await seedProjects();
    await seedTypes();
    process.exit(0);
  } catch (e) {
    console.log('Error in creating Tables:', e);
  }
};

const seedClients = async () => {
  const clients = await Client.findAll({ where: { isArchived: false } });
  for (const client of clientJson) {
    if (!clients.find((c) => c.email === client.email)) {
      await Client.create(client);
    }
  }
  console.log('Clients seeded successfully.');
};

const seedUsers = async () => {
  const users = await User.findAll({ where: { isArchived: false } });
  for (const user of userJson) {
    if (!users.find((u) => u.email === user.email)) {
      await User.create(user);
    }
  }
  console.log('Users seeded successfully.');
};
const seedPriorities = async () => {
  const priorities = await Priority.findAll({ where: { isArchived: false } });
  for (const priority of priorityJson) {
    if (!priorities.find((p) => p.name === priority.name)) {
      await Priority.create(priority);
    }
  }
  console.log('Priorities seeded successfully.');
};
const seedProjects = async () => {
  const projects = await Project.findAll({ where: { isArchived: false } });
  for (const project of projectJson) {
    if (!projects.find((p) => p.name === project.name)) {
      await Project.create(project);
    }
  }
  console.log('Priorities seeded successfully.');
};
const seedStatuses = async () => {
  const statuses = await Status.findAll({ where: { isArchived: false } });
  for (const status of statusJson) {
    if (!statuses.find((s) => s.name === status.name)) {
      await Status.create(status);
    }
  }
  console.log('Statuses seeded successfully.');
};

const seedTypes = async () => {
  const types = await Type.findAll({ where: { isArchived: false } });
  for (const type of typeJson) {
    if (!types.find((t) => t.name === type.name)) {
      await Type.create(type);
    }
  }
  console.log('Types seeded successfully.');
};

seedData();
