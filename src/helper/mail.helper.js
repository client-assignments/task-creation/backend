const nodemailer = require('nodemailer');
/**
 * Config
 * */
const config = require('../config');
/**
 * Services
 * */
const Logger = require('./../services/logger');

/**
 * Mail Template
 */
const mailTemplate = require('./mail-templater/taskTemplate');

/**
 * Transport creation
 */
const transporter = nodemailer.createTransport({
  host: 'smtp.sendgrid.net',
  port: 587,
  auth: {
    user: 'apikey',
    pass: config.mailer.sendgridApiKey,
  },
});

/**
 * Send mail to the specified user
 * @param {*} mailObject mail details
 */
const sendMail = (mailObject) => {
  let subject = mailObject.taskTitle;
  if (mailObject.taskTitle.length > 30) {
    subject = mailObject.taskTitle.substring(0, 27).concat('...');
  }

  const date = new Date(`${mailObject.dueDate}`);
  const year = date.getFullYear();
  let month = date.getMonth() + 1;
  let dt = date.getDate();

  if (dt < 10) {
    dt = '0' + dt;
  }
  if (month < 10) {
    month = '0' + month;
  }
  mailObject.dueDate = year + '-' + month + '-' + dt;

  const html = mailTemplate(mailObject);
  return new Promise((resolve, reject) => {
    const mailBody = {
      from: config.mailer.fromAddress,
      to: mailObject.assineeEmail,
      subject: `Task assigned: ${subject}`,
      html: html,
    };

    if (config.mailer.send === 'true') {
      transporter.sendMail(mailBody, (err, info) => {
        if (err) {
          Logger.log.error('Error sending mail:', err.message || err);
          reject(err);
        } else {
          Logger.log.info('Mail sent Successfully:', info);
          resolve(info);
        }
      });
    } else {
      resolve({
        message: 'SkippedSendMail',
        description:
          'The Mailer did not send mail because of the process configs, set "SEND_MAIL"=true in environment to enable mail service',
        mailObject: mailBody,
      });
    }
  });
};

module.exports = {
  sendMail,
};
