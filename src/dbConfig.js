/**
 * Third party libraries
 */
const Sequelize = require('sequelize');
/**
 * Config
 */
const config = require('./config');

/**
 * Sequelize initialization
 */
const sequelize = new Sequelize(
  config.mysql.database,
  config.mysql.username,
  config.mysql.password,
  {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    operatorsAliases: false,
  },
);
module.exports = sequelize;
