/**
 * Third party libraries
 */
const express = require('express');
const router = express.Router();

/**
 * Models
 */
const Status = require('../models/status.model');

/**
 * Services
 */
const Logger = require('./../services/logger');

/**
 * Get all the status details
 */
router.get('/', async (req, res) => {
  try {
    const StatusData = await Status.findAll({ where: { isArchived: false } });
    return res.send(StatusData);
  } catch (e) {
    Logger.log.error('Error in get status data : ', e.message || e);
    res.send('Error in getting Status data');
  }
});

/**
 * Export Router
 */
module.exports = router;
