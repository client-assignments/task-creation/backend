/**
 * Third party libraries
 */
const express = require('express');
const router = express.Router();

/**
 * Mokels
 */
const Project = require('../models/project.model');

/**
 * Services
 */
const Logger = require('./../services/logger');

/**
 * Get all the projects details
 */
router.get('/', async (req, res) => {
  try {
    const projectData = await Project.findAll({ where: { isArchived: false } });
    return res.send(projectData);
  } catch (e) {
    Logger.log.error('Error in get project data : ', e.message || e);
    res.send('Error in getting Project data');
  }
});

/**
 * Export Router
 */
module.exports = router;
