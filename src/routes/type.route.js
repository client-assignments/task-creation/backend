/**
 * Third party libraries
 */
const express = require('express');
const router = express.Router();

/**
 * Models
 */
const Type = require('../models/type.model');

/**
 * Services
 */
const Logger = require('./../services/logger');

/**
 * Get all the type details
 */
router.get('/', async (req, res) => {
  try {
    const typeData = await Type.findAll({ where: { isArchived: false } });
    return res.send(typeData);
  } catch (e) {
    Logger.log.error('Error in get type data : ', e.message || e);
    res.send('Error in getting Type data');
  }
});

/**
 * Export Router
 */
module.exports = router;
