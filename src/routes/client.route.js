/**
 * Third party libraries
 */
const express = require('express');
const router = express.Router();

/**
 * Models
 */
const Client = require('../models/client.model');

/**
 * Services
 */
const Logger = require('./../services/logger');

/**
 * Get all the clients details
 */
router.get('/', async (req, res) => {
  try {
    const clientData = await Client.findAll({ where: { isArchived: false } });
    return res.send(clientData);
  } catch (e) {
    Logger.log.error('Error in get client data : ', e.message || e);
    res.send('Error in getting client data');
  }
});

/**
 * Export Router
 */
module.exports = router;
