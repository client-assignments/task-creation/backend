/**
 * Third party libraries
 */
const express = require('express');
const router = express.Router();

/**
 * Models
 */
const Task = require('../models/task.model');
const Client = require('../models/client.model');
const Priority = require('../models/priority.model');
const Project = require('../models/project.model');
const Status = require('../models/status.model');
const Type = require('../models/type.model');
const User = require('../models/user.model');

/**
 * Service
 */
const Logger = require('./../services/logger');

/**
 * Helper
 */
const MailHelper = require('./../helper/mail.helper');

/**
 * Get all the type details
 */
router.get('/', async (req, res) => {
  try {
    const taskData = await Task.findAll({
      include: [
        {
          model: Client,
          required: true,
          attributes: ['name'],
        },
        {
          model: Status,
          required: true,
          attributes: ['name'],
        },
        {
          model: Project,
          required: true,
          attributes: ['name'],
        },
        {
          model: Priority,
          required: true,
          attributes: ['name'],
        },
        {
          model: Type,
          required: true,
          attributes: ['name'],
        },
        {
          model: User,
          required: true,
          attributes: ['name'],
          as: 'assignee',
        },
        {
          model: User,
          required: true,
          attributes: ['name'],
          as: 'createdBy',
        },
      ],
      where: { isArchived: false },
      order: [['createdAt', 'DESC']],
    });
    return res.send(taskData);
  } catch (e) {
    Logger.log.error('Error in get task data : ', e);
    res.send('Error in getting Task data');
  }
});

/**
 * Create a Task
 */
router.post('/', async (req, res) => {
  try {
    if (
      !req.body ||
      !req.body.createdById ||
      !req.body.taskTitle ||
      !req.body.projectId ||
      !req.body.clientId ||
      !req.body.statusId ||
      !req.body.typeId ||
      !req.body.priorityId ||
      !req.body.dueDate ||
      !req.body.assigneeId ||
      !req.body.taskTitle
    ) {
      Logger.log.error('Required field is missing in create Task.');
      return res.status(400).send({ message: 'Required field is missing.' });
    }
    if (req.body.sendEmail) {
      const promiseArray = [
        User.findByPk(req.body.assigneeId),
        Project.findByPk(req.body.projectId),
        User.findByPk(req.body.createdById),
        Priority.findByPk(req.body.priorityId),
        Client.findByPk(req.body.clientId),
      ];
      const [assignee, project, createdBy, priority, client] =
        await Promise.all(promiseArray);

      const mailObject = {
        assineeEmail: assignee.email,
        taskTitle: req.body.taskTitle,
        assigneeName: assignee.name,
        projectName: project.name,
        clientName: client.name,
        createdByName: createdBy.name,
        priorityName: priority.name,
        dueDate: req.body.dueDate,
      };

      await MailHelper.sendMail(mailObject);
    }
    req.body.createdAt = new Date();
    req.body.updatedAt = new Date();
    const taskData = await Task.create(req.body);
    return res.send(taskData);
  } catch (e) {
    Logger.log.error('Error in creating task : ', e.message || e);
    res.send('Error in creating Task');
  }
});

/**
 * Export Router
 */
module.exports = router;
