/**
 * Third party libraries
 */
const express = require('express');
const router = express.Router();

/**
 * Models
 */
const User = require('../models/user.model');

/**
 * Services
 */
const Logger = require('../services/logger');

/**
 * Get all the users details
 */
router.get('/', async (req, res) => {
  try {
    const userData = await User.findAll({ where: { isArchived: false } });
    return res.send(userData);
  } catch (e) {
    Logger.log.error('Error in get users data : ', e.message || e);
    res.send('Error in getting User data');
  }
});

/**
 * Export Router
 */
module.exports = router;
