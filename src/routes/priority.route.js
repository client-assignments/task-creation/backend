/**
 * Third party libraries
 */
const express = require('express');
const router = express.Router();

/**
 * Modles
 */
const Priority = require('../models/priority.model');

/**
 * Services
 */
const Logger = require('./../services/logger');

/**
 * Get all the priority details
 */
router.get('/', async (req, res) => {
  try {
    const PriorityData = await Priority.findAll({
      where: { isArchived: false },
    });
    return res.send(PriorityData);
  } catch (e) {
    Logger.log.error('Error in get Priority data : ', e.message || e);
    res.send('Error in getting Priority data');
  }
});

/**
 * Export Router
 */
module.exports = router;
