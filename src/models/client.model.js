/**
 * Third party libraries
 */
const Sequelize = require('sequelize');
/**
 * DB configs
 */
const sequelize = require('../dbConfig');
/**
 * Models
 */
const Task = require('./task.model');

const Client = sequelize.define(
  'client',
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    name: { type: Sequelize.STRING, allowNull: false },
    email: { type: Sequelize.STRING, allowNull: false },
    phoneNumber: { type: Sequelize.STRING, allowNull: true },
    createdAt: { type: Sequelize.DATE, defaultValue: new Date() },
    updatedAt: { type: Sequelize.DATE, defaultValue: new Date() },
    isArchived: { type: Sequelize.BOOLEAN, defaultValue: false },
  },
  { underscored: false },
);

Client.hasMany(Task, {
  foreignKey: 'clientId',
});

Task.belongsTo(Client);
module.exports = Client;
