/**
 * Third party libraries
 */
const Sequelize = require('sequelize');
/**
 * DB configs
 */
const sequelize = require('../dbConfig');
/**
 * Models
 */
const Task = require('./task.model');

const Project = sequelize.define(
  'project',
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    name: { type: Sequelize.STRING, allowNull: false },
    createdAt: { type: Sequelize.DATE, defaultValue: new Date() },
    updatedAt: { type: Sequelize.DATE, defaultValue: new Date() },
    isArchived: { type: Sequelize.BOOLEAN, defaultValue: false },
  },
  { underscored: false },
);

Project.hasMany(Task, {
  foreignKey: 'projectId',
});

Task.belongsTo(Project);

module.exports = Project;
