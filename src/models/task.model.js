/**
 * Third party libraries
 */
const Sequelize = require('sequelize');
/**
 * DB configs
 */
const sequelize = require('../dbConfig');
/**
 * Models
 */
const User = require('./user.model');

const Task = sequelize.define(
  'task',
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    dueDate: { type: Sequelize.DATE, allowNull: true },
    clientId: {
      type: Sequelize.UUID,
      allowNull: false,
      foreignKey: true,
      references: { model: 'clients', key: 'id' },
    },
    projectId: {
      type: Sequelize.UUID,
      allowNull: false,
      foreignKey: true,
      references: { model: 'projects', key: 'id' },
    },
    statusId: {
      type: Sequelize.UUID,
      allowNull: false,
      foreignKey: true,
      references: { model: 'statuses', key: 'id' },
    },
    typeId: {
      type: Sequelize.UUID,
      allowNull: false,
      foreignKey: true,
      references: { model: 'types', key: 'id' },
    },
    priorityId: {
      type: Sequelize.UUID,
      allowNull: false,
      foreignKey: true,
      references: { model: 'priorities', key: 'id' },
    },
    taskTitle: { type: Sequelize.STRING, allowNull: false },
    notes: { type: Sequelize.STRING, allowNull: true },
    emailNotes: { type: Sequelize.STRING, allowNull: true },
    projectDateFilter: { type: Sequelize.BOOLEAN, defaultValue: false },
    createdAt: { type: Sequelize.DATE, defaultValue: new Date() },
    updatedAt: { type: Sequelize.DATE, defaultValue: new Date() },
    isArchived: { type: Sequelize.BOOLEAN, defaultValue: false },
  },
  { underscored: false },
);
Task.belongsTo(User, { as: 'assignee' });
Task.belongsTo(User, { as: 'createdBy' });
module.exports = Task;
