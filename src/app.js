/**
 * System and 3rd party libs
 */
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
const fs = require('fs');
const cors = require('cors');

/**
 * Required Services
 */
let Logger = require('./services/logger');

/**
 * Global declarations
 */
let models = path.join(__dirname, 'models');

/**
 * Bootstrap Models
 */
fs.readdirSync(models).forEach((file) => require(path.join(models, file)));

/**
 * Bootstrap App
 */
let app = express();

//CORS
app.use(
  cors({
    origin: true,
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: [
      'Origin',
      ' X-Requested-With',
      ' Content-Type',
      ' Accept ',
      ' Authorization',
    ],
    credentials: true,
  }),
);
app.use(Logger.morgan);
app.use(bodyParser.json({ limit: '50mb' }));
app.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: false,
  }),
);
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'upload')));

/**
 * Import and Register Routes
 */
let index = require('./routes');
let user = require('./routes/user.route');
let client = require('./routes/client.route');
let project = require('./routes/project.route');
let priority = require('./routes/priority.route');
let type = require('./routes/type.route');
let task = require('./routes/task.route');
let status = require('./routes/status.model');

app.use('/', index);
app.use('/users', user);
app.use('/clients', client);
app.use('/projects', project);
app.use('/priorities', priority);
app.use('/tasks', task);
app.use('/types', type);
app.use('/statuses', status);

/**
 * Catch 404 routes
 */
app.use(function (req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/**
 * Error Handler
 */
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.json(err);
});

module.exports = app;
