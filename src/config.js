module.exports = {
  baseUrl: process.env.BASE_URL,
  server: {
    port: process.env.PORT || 3000,
    logLevel: process.env.LOG_LEVEL || 'all',
    alertLogLevel: process.env.ALERT_LOG_LEVEL || 'all',
  },
  environment: process.env.ENVIRONMENT || 'production',
  mysql: {
    database: process.env.DATABASE,
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
  },
  mailer: {
    fromAddress: process.env.FROM_EMAIL_ADDRESS || 'no-reply@kevit.io',
    sendgridApiKey: process.env.SENDGRID_API_KEY,
    send: process.env.SEND_MAIL || true,
  },
  frontendUrl: process.env.FRONTEND_URL,
};
