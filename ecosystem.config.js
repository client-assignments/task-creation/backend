module.exports = {
  apps: [
    {
      name: 'client-demo-task-management',
      script: './src/bin/www',
      autorestart: true,
      watch: false,
    },
  ],
};
