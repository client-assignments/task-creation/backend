# Task Management Backend Middleware

## Introduction

- Purpose of project
  - A Task Management Backend Middleware to store Task and its associated data.
- What we achieved with this project
  - A simpler Task Management System with various associated entities stored and presented along with it.

#### 1.1 Purpose of this module

- This is the main core of the project. It connects Web UI, Database and, Mailer service.
- All the API calls are pointed here.

#### 1.2 Module features

- All logical operations are performed here.
- A received Task is created into the Database.
- Attributes of the task (like Status, Priority, etc.) are considered as a separate entity to give dynamicity to users to have their custom values defined.

#### 1.3 Module technical stack

- **Server runtime** : NodeJS v14.x
- **Database** : MySQL 8.x
- **Mail service** : Sendgrid

#### 1.4 Module configuration

-
- Configure SendGrid account
  - Log into your [SendGrid](https://app.sendgrid.com/login/) account.
  - From the left side menu, click on **Settings**, then on **API Keys**.
  - Click the Create API Key button on the top-right of the page to create a new API key. A new screen will appear. Inside the API Key Name field, enter some relevant name (like task management).
  - In the API Key Permissions section, select either Full Access or Restricted Access. If you decide to restrict access, be sure that the key has access to the Mail Send option.
  - Click the Create & View button. You will be presented with your SendGrid API key.
  - For verification, follow the steps for Single Sender Verification from SendGrid [guide](https://sendgrid.com/docs/ui/sending-email/sender-verification)
  - Paste the API Key to the file `.env` in: `SENDGRID_API_KEY` and the sender email address to the file `.env` in: `FROM_EMAIL_ADDRESS`

#### 1.5 Get up and running

- Install requirements

  - Open the terminal from root folder of this repository
  - Run `npm i` to install all the dependencies
  - Currently, the backend server will start on PORT 3555, which is configurable

- Create Database & Database User
  - Create a Database, Database User and grant all privileges on this Database to this created User.
  - Paste Database related configs in the .env file
  - Once done, run `npm start`, and the server starts running on port 3555 (specified in env file)

- Create Database Structure
  - As the Database created in above step is empty and has no Tables, run `npm run generate:schema` the below command which creates Database Tables as per the definitions in the `src/models/*` files.

- Seed Data
  - Once the Database structure is created, *default values* to the associated modules should be added in order for the Task to be created.
  - So run `npm run seed` to seed default values for associated modules like priority, status, task-type, etc & some dummy values for Clients, Projects & Users as well.

- Start the Server
  - Once the above steps are performed correctly, run `npm start`, and the Backend Middleware runs on port `PORT` configured in `.env` file.
